import WeatherApp from "./Components/Weather/Weatherapp";

function App() {
  return (
    <div className="App">
      <WeatherApp />
    </div>
  );
}

export default App;
