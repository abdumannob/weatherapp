import React, { useState } from "react";
import fonimg from "../Assets/bgweather.jpg";
import search_icon from "../Assets/search.png";
import clear_icon from "../Assets/clear.png";
import cloud_icon from "../Assets/cloud.png";
import drizzle_icon from "../Assets/drizzle.png";
import rain_icon from "../Assets/rain.png";
import snow_icon from "../Assets/snow.png";
import wind_icon from "../Assets/wind.png";
import humidity_icon from "../Assets/humidity.png";
import sunimg from "../Assets/img/sunimg.jpg";
import cloudimg from "../Assets/img/cloudimg.jpg";
import rainimg from "../Assets/img/rainimg.jpg";
import snowimg from "../Assets/img/snowimg.png";
import drizzleimg from "../Assets/img/drizzleimg.jpg";
import tumanimg from "../Assets/img/tuman.jpg";

const WeatherApp = () => {
  const api_key = "e9483814cecd436280f43aabd9d63ea7";

  const [wicon, setWicon] = useState({
    fon: fonimg,
    icon: "",
    namlik: "",
    namiq: "",
    wind: "",
    speed: "",
    temp: "",
    locate: "",
    davlat: "",
    windicon: "",
    humidity: "",
  });

  const weatherimg = {
    "01d": sunimg,
    "01n": sunimg,
    "02d": cloudimg,
    "02n": cloudimg,
    "03d": drizzleimg,
    "03n": drizzleimg,
    "04d": drizzleimg,
    "04n": drizzleimg,
    "09d": rainimg,
    "09n": rainimg,
    "10d": rainimg,
    "10n": rainimg,
    "13d": snowimg,
    "13n": snowimg,
    "50n": tumanimg,
  };

  const weatherIcon = {
    "01d": clear_icon,
    "01n": clear_icon,
    "02d": cloud_icon,
    "02n": cloud_icon,
    "03d": drizzle_icon,
    "03n": drizzle_icon,
    "04d": drizzle_icon,
    "04n": drizzle_icon,
    "09d": rain_icon,
    "09n": rain_icon,
    "10d": rain_icon,
    "10n": rain_icon,
    "13d": snow_icon,
    "13n": snow_icon,
    "50n": humidity_icon,
  };

  const search = async () => {
    const element = document.getElementById("input");
    if (element.value === "") {
      return 0;
    }
    try {
      const url = `https://api.openweathermap.org/data/2.5/weather?q=${element.value}&units=Metric&appid=${api_key}`;
      const response = await fetch(url);
      const data = await response.json();

      setWicon({
        fon: weatherimg[data.weather[0].icon],
        icon: weatherIcon[data.weather[0].icon],
        temp: `${Math.floor(data.main.temp)}°c`,
        locate: data.name,
        davlat: data.sys.country,
        humidity: humidity_icon,
        namlik: `${data.main.humidity} %`,
        namiq: "Namlik",
        windicon: wind_icon,
        wind: `${Math.floor(data.wind.speed)} km/s`,
        speed: "Shamol tezligi",
      });
    } catch (error) {
      console.log("Joylashuv nomini to'g'ri kiriting");
    }
  };

  return (
    <div className="relative">
      <img
        src={wicon.fon}
        className="absolute img z-[-1] top-0 left-0 w-full h-screen"
        alt=""
      />
      <div className="xl:w-1/2 xl:rounded-xl mx-auto">
        <div className="mx-auto pt-16 flex justify-center gap-3">
          <input
            id="input"
            type="text"
            className="flex md:w-4/6 h-16 outline-none bg-bgfon rounded-full ps-10 text-xl"
            placeholder="Search"
          />
          <div
            className="bg-bgfon w-16 h-16 flex items-center rounded-full justify-center cursor-pointer"
            onClick={search}
          >
            <img className="w-16 h-11" src={search_icon} alt="search" />
          </div>
        </div>
        <div className="flex justify-center mt-7">
          <img className="w-40 md:w-64" src={wicon.icon} alt="" />
        </div>
        <div className="flex justify-center text-6xl md:text-8xl text-white shadowe">
          {wicon.temp}
        </div>
        <div className="flex justify-center">
          <h1 className="flex justify-center shadowe text-white text-4xl md:text-6xl">
            {wicon.locate}
          </h1>
          <h6 className="text-white text-xl flex items-end ms-5">
            {wicon.davlat}
          </h6>
        </div>

        <div className="flex justify-center text-white mt-10 font-bold mx-2 bg-opacity-50 rounded-xl">
          <div className="m-auto flex items-start gap-3 ">
            <img src={wicon.humidity} alt="" className="mt-2.5 " />
            <div className="md:text-3xl text-xl">
              <div className="shadowe">{wicon.namlik}</div>
              <div className="text-xl shadowe">{wicon.namiq}</div>
            </div>
          </div>
          <div className="m-auto flex items-start gap-3">
            <img src={wicon.windicon} alt="" className="mt-2.5 " />
            <div className="md:text-3xl text-xl">
              <div className="shadowe">{wicon.wind}</div>
              <div className="shadowe md:text-2xl text-xl">{wicon.speed}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default WeatherApp;
